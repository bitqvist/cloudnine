# README #

This is a techtest for CloudNine

### How do I get set up? ###

Setting up is really easy, just:

1. git clone git@bitbucket.org:bitqvist/cloudnine.git cloudnine
2. cd cloudnine
3. npm install
4. npm run dev

The path to the web root is src/client. 

The code is meant to run on phone devices only, no
layout for tablet or desktop available.

Instead of Helvetica Neue (I didn't have it), Rototo 
(Google Fonts) was used.

The code was developed on:

- Google Chrome
- iOS simulator (iPhone 8, X, SE)

Sample data via json file to simulate a real case where data would 
have been stored globally in a state manager (like Redux). Alternatively,
data could be read each time a filtering is made via fetch api (like Refetch). 

