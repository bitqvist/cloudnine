import React, { Component, PropTypes } from 'react';
import ListItem from '../../components/ListItem/ListItem';
import RetinaImage from 'react-retina-image';
import styles from './ListView.css';
import backButton from '../../images/left_arrow.png';
import backButtonRetina from '../../images/left_arrow@2x.png';
import dropDownButton from '../../images/down_arrow.png';
import settings from '../../images/settings.png';
import settingsRetina from '../../images/settings@2x.png';
import data from '../../data/salons.json';

export default class ListView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: "2",
    };
    this.change = this.change.bind(this);
  }

  change(event){
    this.setState({value: event.target.value});
  }

  render () {

    let tier = this.state.value;

    return(
      <div className={styles.container}>
        <div className={styles.navBar}>
          <RetinaImage className={styles.backButton} src={[backButton, backButtonRetina]} />
          <h3>Hår</h3>
          <RetinaImage className={styles.settings} src={[settings, settingsRetina]} />
        </div>

        <div className={styles.dropDown} >

          <select id={styles.dropDownSelect} className={styles.roboto} onChange={this.change} value={this.state.value} >
              <option value="1" >Pris 1 - 249 kr</option>
              <option value="2" >Pris 250 - 499 kr</option>
              <option value="3" >Pris 500+ kr</option>
            </select>
          <img className={styles.dropDownButton} src={dropDownButton}/>
        </div>

        <div className={styles.salonList}>
          <ul>
            {data.salons.map(function (item) {
              return <ListItem key={item.id} salon={item} tier={tier}/>
            })}
          </ul>
        </div>
      </div>
    )
  }
}