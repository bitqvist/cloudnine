import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router-dom';
import RetinaImage from 'react-retina-image';
import Stars from '../../components/Stars/Stars';
import styles from './SalonView.css';
import sampleSalonImage from '../../salons/salon.jpg';
import sampleSalonImageRetina from '../../salons/salon@2x.jpg';
import backButtonWhite from '../../images/back_button_white.png';
import backButtonWhiteRetina from '../../images/back_button_white@2x.png';
import makeFavorite from '../../images/make_favorite.png';
import makeFavoriteRetina from '../../images/make_favorite@2x.png';
import pin from '../../images/pin.png';
import pinRetina from '../../images/pin@2x.png';
import clock from '../../images/clock.png';
import clockRetina from '../../images/clock@2x.png';
import phone from '../../images/phone.png';
import phoneRetina from '../../images/phone@2x.png';
import www from '../../images/www.png';
import wwwRetina from '../../images/www@2x.png';
import arrowDown from '../../images/down_arrow.png';
import arrowDownRetina from '../../images/down_arrow@2x.png';
import data from '../../data/salons.json';

export default class SalonView extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    const salonId = this.props.match.params.number - 1;
    const salon = data.salons[salonId];

    return (
      <div className={styles.container}>
        <div className={styles.imgContainer}>
          <RetinaImage className={styles.salonImage} src={[sampleSalonImage, sampleSalonImageRetina]} alt={salon.name}/>


          <Link to={{pathname: '/'}} className={styles.backLink}>
            <RetinaImage src={[backButtonWhite, backButtonWhiteRetina]} />
          </Link>

          <Link to={{pathname: ''}} className={styles.makeFavorite}>
            <RetinaImage src={[makeFavorite, makeFavoriteRetina]} />
          </Link>

          <div className={styles.gradient}></div>
          <span className={styles.salonName}>{salon.name}</span>
          <div className={styles.salonStars}>
            <Stars stars={salon.stars}/>
          </div>
          <div className={`${styles.roboto} ${styles.salonRaters}`}>({salon.raters})</div>
        </div>

        <div className={`${styles.roboto} ${styles.fakeTabMenu}`}>
          <span className={styles.info}>Info</span>
          <span className={styles.schema}>Schema</span>
        </div>

        <div className={`${styles.roboto} ${styles.infoBox}`}>

          <div className={styles.infoBoxRow}>
              <RetinaImage src={[pin, pinRetina]} />
            <p>{salon.streetAddress}, {salon.postal_code}</p>
          </div>

          <div className={styles.infoBoxRow}>
            <RetinaImage src={[clock, clockRetina]}/>
            <p style={{ paddingRight: '3%'}} >Öppet till kl 19.00 idag</p>
            <RetinaImage src={[arrowDown, arrowDownRetina]} />
          </div>

          <div className={styles.infoBoxRow}>
            <RetinaImage src={[phone, phoneRetina]}/>
            <p>{salon.phone}</p>
          </div>

          <div className={styles.infoBoxRow}>
            <RetinaImage src={[www, wwwRetina]}/>
            <p>{salon.www}</p>
          </div>

          <div className={styles.infoBoxRow} style={{ lineHeight: '135%'}}>
            <p>{salon.decription}</p>
          </div>

        </div>

      </div>
    )
  }
}
