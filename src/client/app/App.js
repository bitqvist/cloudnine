import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import ListView from './views/ListView/ListView';
import SalonView from './views/SalonView/SalonView';
import WebFont from 'webfontloader';

export default class App extends React.Component {
  render() {

    // loading fonts...
    WebFont.load({
      custom: {
        families: ['MillerBanner-Light'],
        src: ['./App.css']
      },
      google: {
        families: ['Roboto', 'sans-serif']
      }
    });

    return (
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={ListView}/>
          <Route exact path='/list' component={ListView}/>
          <Route exact path='/salon/:number' component={SalonView}/>
        </Switch>
      </BrowserRouter>
    )
  }
}
