import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router-dom';
import Stars from "../Stars/Stars";
import RetinaImage from 'react-retina-image';
import RightArrow from '../../images/right_arrow.png';
import RightArrowRetina from '../../images/right_arrow@2x.png';
import styles from './ListItem.css';



export default class ListItem extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const { salon, tier }  = this.props;

    let min, max;

    if (tier === "1") {
      min = 1;
      max = 249;
    } else if (tier === "2") {
      min = 250;
      max = 499;
    } else if (tier === "3") {
      min = 500;
      max = 100000;
    }

    if (salon.price < min || salon.price > max) {
      return null;
    }

    return (
      <li className={styles.container}>
        <Link to={{ pathname: '/salon/' + salon.id }} >

          <div className={styles.columnOne}>
              <span className={`${styles.roboto} ${styles.available}`}>{salon.available}</span>
          </div>

          <div className={styles.columnTwo}>
            <div className={styles.salonName}> {salon.name}</div>
              <div className={styles.starAndRaters}>
                <Stars stars={salon.stars} />
                <span className={`${styles.roboto} ${styles.raters}`} style={{color: '#656565'}}>({salon.raters})</span>
              </div>
              <div className={`${styles.roboto} ${styles.salonAddress}`} style={{color: '#656565'}}>{salon.streetAddress}</div>
          </div>

          <div className={styles.columnThree}>
              <span className={`${styles.roboto} ${styles.salonPrice}`}> {salon.price} kr</span>
              <span className={`${styles.roboto} ${styles.salonDuration}`} style={{color: '#656565'}}>{salon.duration} min</span>
          </div>

          <div className={styles.columnFour}>
            <RetinaImage src={[RightArrow, RightArrowRetina]} />
          </div>

        </Link>
      </li>
    )
  }
}