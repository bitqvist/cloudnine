import React, { Component, PropTypes } from 'react';
import RetinaImage from 'react-retina-image';
import FilledStar from '../../images/star_filled.png';
import FilledStarRetina from '../../images/star_filled@2x.png';
import EmptyStar from '../../images/star_empty.png';
import EmptyStarRetina from '../../images/star_empty@2x.png';
import styles from './Stars.css';

export default class Stars extends Component {

  constructor(props) {
    super(props);

    this.createStarHtml = this.createStarHtml.bind(this);
  }


  createStarHtml(stars) {

    const starsArr = [];

    for (let i = 0; i < 5; i++) {
      if (i < stars) {
        starsArr.push({
          filled: true,
        });
      } else {
        starsArr.push({
          filled: false,
        });
      }

    }

    return starsArr;
  }


  render () {
    const { stars } = this.props;
    const starsArr = this.createStarHtml(stars);

    return (
      <div className={styles.stars}>
        {starsArr.map(function (item, i) {
          return (
            <div key={i} className={styles.star}>
              <RetinaImage src= { item.filled === true ? [FilledStar, FilledStarRetina] : [EmptyStar, EmptyStarRetina] } />
            </div>
          )
        })}
      </div>
    )
  }
}